# -*- encoding: utf-8 -*-

{
    'name': 'Tosamaganga - Products',
    'version': '0.9',
    'author': 'Alessandro Domanico for ISF',
    'description': """
Tosamaganga Hospital - MDS Products
================================================

Tosamaganga Hospital - MSD Products Catalogue 2014-2015.
    """,
    'license': 'AGPL-3',
    'category': 'Localization',
#     'depends': [
#             'base',
#             'account',
#             'account_budget',
#             'account_voucher',
#             'account_cancel',
#             'stock',
#             'product_expiry',
#             'account_asset',
#             'sale',
#             'purchase',
#             'hr',
#             'hr_contract',
#             'hr_payroll',
#             'hr_payroll_account',
#             'purchase',
#             'stock',
#             'stock_location',
#             #'isf_home_dashboard',
#             #'isf_loginmanager',
#             'web_m2o_enhanced',
#             'web_pdf_preview',
#             'web_printscreen_zb',
#             'ons_webkit_report',
#             'isf_cims_module',
#             'product_fifo_lifo',
#     ],
    'data': [
		#'security/ir.model.access.csv', <-- file present just as example
        'base_data.xml',
        'data/ir.model.data.csv',
        'data/product.category.csv',
        'data/product.uom.csv',
        'data/product.product.csv',
        'data/stock.location.csv',
        'isf_cims_module_autoconfig.xml',
        ],
    'demo': [],
    'installable': True,
    'auto_install': False
}
