# -*- coding: utf-8 -*-
import time
from datetime import datetime
from dateutil.relativedelta import relativedelta

import pprint
from xlwt.ExcelFormulaParser import FALSE_CONST
pp = pprint.PrettyPrinter(indent=4)

from openerp.osv import fields, orm

from openerp.report import report_sxw
from openerp import tools
import logging
import locale
import platform
from collections import OrderedDict

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

    
_logger = logging.getLogger(__name__)
_debug = True

class isf_tosa_asset_revision_book_parser(report_sxw.rml_parse):
    _name = 'report.isf.tosa.asset.revision.book.webkit'
    
    def __init__(self, cr, uid, name, context=None):
        super(isf_tosa_asset_revision_book_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'datelines' : self.datelines,
        })
        self.context = context
        self.result_date = []
        self.result_values = []
    
    def _get_company_currency(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
        
        return currency_id
        
    def datelines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.tosa.asset.revision.book')
        report_fiscal_year = obj_report.read(self.cr, self.uid, ctx['active_id'], ['fiscalyear_id'])
        report_group_by = obj_report.read(self.cr, self.uid, ctx['active_id'], ['group_by'])
        report_asset_category_ids = obj_report.read(self.cr, self.uid, ctx['active_id'], ['asset_category_ids'])
        report_location_ids = obj_report.read(self.cr, self.uid, ctx['active_id'], ['location_ids'])
        report_page_break = obj_report.read(self.cr, self.uid, ctx['active_id'], ['page_break'])
        currency = self._get_company_currency(self.cr, self.uid)
        
        fiscal_year = report_fiscal_year['fiscalyear_id']
        group_by = report_group_by['group_by']
        asset_category_ids = report_asset_category_ids['asset_category_ids']
        location_ids = report_location_ids['location_ids']
        page_break = report_page_break['page_break']
        
        if _debug:
            _logger.debug('fiscal_year: %s', fiscal_year)
            _logger.debug('group_by: %s', group_by)
            _logger.debug('asset_category_ids: %s', asset_category_ids)
            _logger.debug('location_ids: %s', location_ids)
            _logger.debug('page_break: %s', page_break)
        
        res = {
            'fiscal_year' : fiscal_year[1],
            'group_by' : group_by,
            'asset_category_ids' : asset_category_ids,
            'location_ids' : location_ids,
            'currency' : currency.name,
            'page_break' : page_break,
        }
        
        self.result_date.append(res)
        
        return self.result_date
    
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.tosa.asset.revision.book')
        report_fiscal_year = obj_report.read(self.cr, self.uid, ctx['active_id'], ['fiscalyear_id'])
        report_group_by = obj_report.read(self.cr, self.uid, ctx['active_id'], ['group_by'])
        report_asset_category_ids = obj_report.read(self.cr, self.uid, ctx['active_id'], ['asset_category_ids'])
        report_location_ids = obj_report.read(self.cr, self.uid, ctx['active_id'], ['location_ids'])
        
        fiscal_year = report_fiscal_year['fiscalyear_id']
        group_by = report_group_by['group_by']
        asset_category_ids = report_asset_category_ids['asset_category_ids']
        location_ids = report_location_ids['location_ids']
        
        if _debug:
            _logger.debug('fiscal_year: %s', fiscal_year)
            _logger.debug('group_by: %s', group_by)
            _logger.debug('asset_category_ids: %s', asset_category_ids)
            _logger.debug('location_ids: %s', location_ids)
        
        obj_asset = self.pool.get('account.asset.asset')
        if group_by == 'category':
            asset_ids = obj_asset.search(self.cr, self.uid, [('category_id','in',asset_category_ids)], order="category_id asc, code asc")
        else:
            asset_ids = obj_asset.search(self.cr, self.uid, [('location_id','in',location_ids)], order="location asc, code asc")
        
        previous_group = ''
        for asset in obj_asset.browse(self.cr, self.uid, asset_ids):
            if group_by == 'category':
                if previous_group != asset.category_id.name:
                    previous_group = asset.category_id.name
                    res = {}
                    res['type'] = 'caption'
                    res['caption'] = asset.category_id.name
                    self.result_values.append(res)
            else:
                if previous_group != asset.location_id.name:
                    previous_group = asset.location_id.name
                    res = {}
                    res['type'] = 'caption'
                    res['caption'] = asset.location_id.name
                    self.result_values.append(res)
                    
            res = {}
            res['type'] = 'asset'
            res['id'] = asset.id
            res['category'] = asset.category_id.name
            res['location'] = asset.location_id.name
            res['code'] = asset.code
            res['old_code'] = asset.old_code or ''
            res['name'] = asset.name
            res['condition'] = asset.condition
            res['date_start'] = asset.date_start
            res['purchase_value'] = locale.format("%0.0f", asset.purchase_value, grouping=True)
            res['value_residual'] = locale.format("%0.0f", asset.value_residual, grouping=True)
            self.result_values.append(res)
            
        if _debug:
            _logger.debug('self.result_values: %s', self.result_values)
            
        return self.result_values
    
report_sxw.report_sxw('report.isf.tosa.asset.revision.book.webkit', 'isf.tosa.asset.revision.book', 'addons/isf_tosa_asset/report/isf_tosa_asset_revision_book.mako', parser=isf_tosa_asset_revision_book_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
