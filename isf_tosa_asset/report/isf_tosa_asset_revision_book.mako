<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        ${css}
        
        body {
		    font-size: x-small;
		}

		table {
		    border-spacing: 0px;
		    page-break-inside: auto;
		    table-layout: fixed;
		}
		
		table tr {
            page-break-inside: avoid; 
            page-break-after: auto;
            line-height: 20px;
  		}
  		
		tr.space td {
  			line-height: 20px;
		}
		
		td {
			padding: 1px;
		}
		
		.values {
			font-size: x-small;
			text-align: right;
			border: 1px solid black;
		}
		
		.dates {
			text-align: center;
			font-size: x-small;
			border: 1px solid black;
		}
		
		.labels {
			font-size: x-small;
			text-align: left;
			overflow: hidden;
			text-overflow: ellipsis;
			border: 1px solid black;
		}
		
		.labels_center {
			font-size: x-small;
			text-align: center;
			overflow: hidden;
			text-overflow: ellipsis;
			border: 1px solid black;
		}
		
		.category {
			font-size: xx-large;
			line-height: 50px;
			font-weight: bold;
		}
		
		.asset {
			font-size: x-small;
			line-height: 20px;
			border: 1px solid black;
		}
		
		.header {
			font-size: x-small;
			line-height: 20px;
			font-weight: bold;
			border: 1px solid black;
		}
		
		.table_header {
			font-size: x-small;
			line-height: 20px;
			font-weight: bold;
		}
		
		.page-break{
			page-break-after:always
		} 
		
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <br />
    <h1><b><i><center>Fixed Assets Revision Book Form</center></i></b></h1>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print datelines()
    print lines()
    """
    
    first_page = True
    local_datelines = datelines()
    for dateline in local_datelines:
    	if dateline['group_by'] == 'category':
    		group_criteria = 'category'
    	if dateline['group_by'] == 'location':
    		group_criteria = 'location'
    	page_break = dateline['page_break']
    %>
	
	%for date in local_datelines:
    	<h2>Fiscal Year: ${date['fiscal_year']} (Values: ${date['currency']})</h2>
    	<h2>Group by: ${date['group_by']}</h2>
    %endfor
    
    <table style="width: 100%;">
    	<tr>
        	<td width="3%"></td>
    		<td width="8%"></td>
            <td width="4%"></td>
            <td width="12%"></td>
            <td width="22%"></td>
            <td width="7%"></td>
            <td width="7%"></td>
            <td width="7%"></td>
            <td width="7%"></td>
            <td width="5%"></td>
            <td width="8%"></td>
            <td width="8%"></td>
        </tr>
    %for line in lines():
	    %if line['type'] == 'caption':
	    	%if not first_page and page_break:
		    	</table>
		    	<div class="page-break"></div>
		    	<table style="width: 100%;">
		    	<tr>
		        	<td width="3%"></td>
		    		<td width="8%"></td>
		            <td width="4%"></td>
		            <td width="12%"></td>
		            <td width="22%"></td>
		            <td width="7%"></td>
		            <td width="7%"></td>
		            <td width="7%"></td>
		            <td width="7%"></td>
		            <td width="5%"></td>
		            <td width="8%"></td>
		            <td width="8%"></td>
		        </tr>
		    %else:
		    	<% 
		    		first_page = False 
		    	%>
		    %endif
	    	<tr class="category">
	    		<td colspan='11'>${line['caption']}</td>
	    	</tr>
	    	<tr class="header">
	        	<td class="labels">ID</td>
	        	%if group_criteria == 'category':
	        		<td class="labels">Location</td>
	        	%else:
					<td class="labels">Category</td>
				%endif
	            <td class="labels">Code</td>
	            <td class="labels">Old Code</td>
	            <td class="labels">Name</td>
	            <td class="labels_center">Condition</td>
	            <td class="labels_center">Date Start</td>
	            <td class="values">Historical Value</td>
	            <td class="values">Residual Value</td>
	            <td class="labels_center">Physical Check</td>
	            <td class="labels">Variances books / physical</td>
	            <td class="labels">Remarks</td>
	        </tr>
	    %else:
			<tr class="asset">
	    		<td class="labels">${line['id']}</td>
	    		%if group_criteria == 'category':
	        		<td class="labels">${line['location']}</td>
	        	%else:
					<td class="labels">${line['category']}</td>
				%endif
	    		<td class="labels">${line['code']}</td>
	    		<td class="labels">${line['old_code']}</td>
	    		<td class="labels">${line['name']}</td>
	    		<td class="labels_center">${line['condition']}</td>
	    		<td class="labels_center">${line['date_start']}</td>
	    		<td class="values">${line['purchase_value']}</td>
	    		<td class="values">${line['value_residual']}</td>
	    		<td class="labels_center"></td>
	    		<td class="labels"></td>
	    		<td class="labels"></td>
	    	</tr>
	    %endif
    %endfor
    </table>
</body>
</html>
