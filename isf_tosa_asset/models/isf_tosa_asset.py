from openerp.osv import fields, orm, osv
from openerp import SUPERUSER_ID
import logging
import time
from openerp import tools
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)
_debug = False

class isf_account_asset_category(orm.Model):
    _inherit = 'account.asset.category'

    _columns = {
        'code': fields.char('Prefix', size=5, required=True, help="The prefix used for the entry sequence."),
        'sequence_id': fields.many2one('ir.sequence', 'Entry Sequence', required=True, help="This field contains the information related to the numbering of the asset entries of this category."),
    }

    def create_sequence(self, cr, uid, vals, context=None):
        """ Create new no_gap entry sequence for every new Asset Category
        """
        # in account.asset.category code is actually the prefix of the sequence
        # whereas ir.sequence code is a key to lookup global sequences.
        prefix = vals['code'].upper()

        seq = {
            'name': vals['name'],
            'implementation':'no_gap',
            'prefix': prefix,
            'padding': 4,
            'number_increment': 1
        }
        if 'company_id' in vals:
            seq['company_id'] = vals['company_id']
        return self.pool.get('ir.sequence').create(cr, uid, seq)


    def create(self, cr, uid, vals, context=None):
        if vals.get('method_time') != 'year' and not vals.get('prorata'):
            vals['prorata'] = True
        if not 'sequence_id' in vals or not vals['sequence_id']:
            # if we have the right to create an Asset Category, we should be able to
            # create it's sequence.
            vals.update({'sequence_id': self.create_sequence(cr, SUPERUSER_ID, vals, context)})
        categ_id = super(isf_account_asset_category, self).create(
            cr, uid, vals, context=context)
        acc_obj = self.pool.get('account.account')
        acc_id = vals.get('account_asset_id')
        if acc_id:
            account = acc_obj.browse(cr, uid, acc_id)
            if not account.asset_category_id:
                acc_obj.write(
                    cr, uid, [acc_id], {'asset_category_id': categ_id})
        return categ_id
    

class isf_tosa_asset(orm.Model):
    _inherit = 'account.asset.asset'
    
    def _check_unique_insesitive(self, cr, uid, ids, context=None):

        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.old_code and self.search_count(cr, uid, [('old_code', 'ilike', self_obj.old_code), ('id', '!=', self_obj.id)], context=context) != 0:
                return False
    
        return True
    
    _columns = {
        'code': fields.char(
            'Reference', size=32, readonly=True,
            states={'draft': [('readonly', True)]}), # made readonly, it will be populated by the category's sequence
        'old_code': fields.char('Old Reference', size=32),
        'condition': fields.selection([
            ('fair', 'Fair, Still Useful'),
            ('good', 'Good, Running'),
            ('useful', 'Good, Useful'),
            ('verygood', 'Very Good, Almost New'),
            ('new', 'New, the Asset is new'),
            ('grounded', 'Poor, Grounded'),
            ('poor', 'Poor, Running'),
            ('replace', 'Poor, Needs Replacement'),
            ('repairable', 'Poor, Repairable'),
            ('verypoor', 'Very Poor, To scrap'),
            ], 'State of Use',
            help="Goods Condition"),
        'location': fields.related(
            'location_id', 'name',
            string='Location Name',
            type='char',
            relation='isf.tosa.asset.location',
            store=True, readonly=True,),
        'location_id': fields.many2one('isf.tosa.asset.location','Location ID',ondelete='restrict'),
        'serial_number': fields.char('Serial Number', size=64),
    }
    
    _sql_constraints = [(
        'code_unique', 'unique(code)', 'Reference already exists')]
    
    #_constraints = [(_check_unique_insesitive, _('The old_code must be unique!'), ['old_code'])]
    
    _defaults = {
        'code': '/'
    }
    
    def validate(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        currency_obj = self.pool.get('res.currency')
        obj_sequence = self.pool.get('ir.sequence')
        for asset in self.browse(cr, uid, ids, context=context):
            if asset.type == 'normal' and currency_obj.is_zero(
                    cr, uid, asset.company_id.currency_id,
                    asset.value_residual):
                asset.write({'state': 'close'}, context=context)
            else:
                asset.write({'state': 'open'}, context=context)
            
            if asset.code == '/':    
                new_name = False
                category = asset.category_id
                
                if category.sequence_id:
                    new_name = obj_sequence.next_by_id(cr, uid, category.sequence_id.id)
                else:
                    raise osv.except_osv(_('Error!'), _('Please define a sequence on the Asset Category.'))
                
                if new_name:
                    asset.write({'code':new_name}, context=context)
                    
        return True
    
class isf_asset_multiple_validate(osv.osv_memory):
    _name = 'isf.assets.multiple.validate'
    
    def multiple_validate(self, cr, uid, ids, context=None):
        asset_obj = self.pool.get('account.asset.asset')
        active_ids = context['active_ids']
        
        asset_obj.validate(cr, uid, active_ids, context=context)
            
        return True
    
class isf_asset_multiple_compute(osv.osv_memory):
    _name = 'isf.assets.multiple.compute'
    
    def multiple_compute(self, cr, uid, ids, context=None):
        asset_obj = self.pool.get('account.asset.asset')
        active_ids = context['active_ids']
        
        assets = asset_obj.browse(cr, uid, active_ids)
        for asset in assets:
            if asset.type != 'view':
                asset_obj.compute_depreciation_board(cr, uid, [asset.id], context=context)
            
        return True
    
class isf_asset_multiple_revaluate(osv.osv_memory):
    _name = 'isf.assets.multiple.revaluate'
    
    _columns = {
        'percentage' : fields.float('Percentage', digits=(3,2)),
        'date_revaluation': fields.date('Date', required=True),
    }
    
    _defaults = {
        'percentage' : 100.0,
    }
    
    def _check_percentage_value(self, cr, uid, ids, context=None):
        for revaluation in self.browse(cr, uid, ids, context=context):
            if revaluation.percentage < 0 or revaluation.percentage == 100:
                return False
        return True
    
    _constraints = [(
        _check_percentage_value,
        "The percentage cannot be 100 or negative!",
        ['percentage']
    )]
    
    def multiple_revaluate(self, cr, uid, ids, context=None):
        asset_obj = self.pool.get('account.asset.asset')
        asset_revaluation_obj = self.pool.get('account.asset.revaluation.wizard')
        active_ids = context['active_ids']
        wiz_data = self.browse(cr, uid, ids[0], context=context)
        percentage = wiz_data.percentage
        
        if _debug:
            _logger.debug('==> active_ids : %s', active_ids)
            _logger.debug('==> percentage : %s', percentage)
            _logger.debug('==> date_revaluation : %s', wiz_data.date_revaluation)
        
        for asset in asset_obj.browse(cr, uid, active_ids):
            if asset.type == 'view':
                context['active_ids'] = [d['id'] for d in asset.child_ids]
                if _debug:
                    _logger.debug('==> asset.child_ids : %s', context['active_ids'])
                self.multiple_revaluate(cr, uid, ids, context)
            else:
                if _debug:
                    _logger.debug('==> asset.id : %s', asset.id)
                    _logger.debug('==> asset.name : %s', asset.name)
                    _logger.debug('==> asset.purchase_value : %s', asset.purchase_value)
                    _logger.debug('==> asset.asset_value : %s', asset.asset_value)
                    _logger.debug('==> asset.value_residual : %s', asset.value_residual)
                
                context['active_id'] = asset.id
                context['early_removal'] = True
                vals = {
                    'date_revaluation': wiz_data.date_revaluation,
                    'revaluated_value': asset.asset_value * percentage / 100.0,
                    'note': 'test',
                }
                asset_revaluation_id = asset_revaluation_obj.create(cr, uid, vals, context)
                asset_revaluation_obj.revaluate(cr, uid, [asset_revaluation_id], context)
        
        #asset_obj.validate(cr, uid, active_ids, context=context)
            
        return True
