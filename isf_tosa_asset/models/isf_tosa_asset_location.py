from openerp.osv import fields, orm, osv
import logging
import time
from openerp import tools
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)
_debug = False

class isf_tosa_asset_location(orm.Model):
    _name = 'isf.tosa.asset.location'
    _description = 'Asset Location'
    _order = 'name'
    
    def _check_unique_insesitive(self, cr, uid, ids, context=None):

        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self.search_count(cr, uid, [('name', 'ilike', self_obj.name), ('id', '!=', self_obj.id)], context=context) != 0:
                return False
    
        return True
    
    _columns = {
        'parent_location_id' : fields.many2one('isf.tosa.asset.location','Parent Location'),
        'name' : fields.char('Name', required=True),
    }
    
    _sql_constraints = [(
        'name', 'unique(name)', 'Location already exists!')]
    
    _constraints = [(_check_unique_insesitive, _('Location already exists!'), ['name'])]
    
    def _insertUpdateLocations(self, cr, uid, ids=None, context=None):
        context = context or {}
        module_update_obj = self.pool.get('isf.module.update')
        updated = True
        if not module_update_obj.is_updated(cr, uid, __name__, _debug, context):
            if _debug:
                _logger.debug('==> Updating...')
            locations = cr.execute("SELECT DISTINCT(location) FROM account_asset_asset")
            locations = cr.fetchall()
            for location in locations:
                if _debug:
                    _logger.debug('==> location : %s', location[0])
                if location[0]:
                    id = 0
                    try:
                        cr.execute("INSERT INTO isf_tosa_asset_location(create_uid, create_date, write_date, write_uid, name) VALUES (1, (now() at time zone 'UTC'), (now() at time zone 'UTC'), 1, %s) RETURNING id", (location))
                        id = cr.fetchone()[0]
                        _logger.debug('==> location_id : %s', id)
                        cr.execute("UPDATE account_asset_asset SET location_id = %s WHERE location = %s", (id, location[0]))
                    except Exception, ex:
                        _logger.exception('==> handled Exception : %s', ex)
                        updated = False
            cr.commit()
            if updated:
                module_update_obj.module_updated(cr, uid, __name__, _debug, context)
        else:
            if _debug:
                _logger.debug('==> Already updated!')
        return True
    
    def write(self, cr, uid, ids, vals, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        
        location_obj = self.pool.get('isf.tosa.asset.location')
        location_ids = location_obj.search(cr, uid, [('id','in',ids)])
        for location in location_obj.browse(cr, uid, location_ids):
            try:
                if _debug:
                    _logger.debug('==> old location name: %s', location.name)
                    _logger.debug('    new location name: %s', vals.get('name'))
                cr.execute("UPDATE account_asset_asset SET location = %s WHERE location_id = %s", (vals.get('name'), location.id))
            except Exception, ex:
                _logger.exception('==> handled Exception : %s', ex)
        
        return super(isf_tosa_asset_location, self).write(cr, uid, ids, vals, context=context)
    
isf_tosa_asset_location()