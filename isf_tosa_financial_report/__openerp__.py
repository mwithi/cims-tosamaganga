# -*- coding: utf-8 -*-

{
	'name': 'ISF Tosa Financial Report',
	'version': '1.0',
	'category': 'Tools',
	'description': """

ISF Tosa Financial Report 
==================================

Add the Financial Statement Report for Tosamaganga:

""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': ['isf_tosa_coa'],
	'data': [
            'data/account.financial.report.csv',
            'isf_financial_report.xml',
    	],
	'demo': [],
	'installable' : True,
}

