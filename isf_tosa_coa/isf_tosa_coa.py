from openerp.osv import osv
import logging

logger = logging.getLogger()

class isf_tosa_coa(osv.osv):
    _name = "isf.tosa.coa"
    _auto = False

    def init(self, cr):
        #logger.info("*** isf_tosa_coa.init CALLED! ***")
        try:
            self.deleteDefaultAccountingTemplates(cr)
        except Exception, ex:
            logger.exception(ex)
        # logger.info("########### CREATING ACCOUNT KEYS")
        ids_c = cr.execute("select id, code from account_account")
        ids = cr.fetchall()
        for acdata in ids:
            aid, acode = acdata
            name_id = acode
            model = "account.account"
            module = "isf_tosa_coa"
            cr.execute('select * from ir_model_data where name=%s and module=%s', (name_id, module))
            if not cr.rowcount:
                cr.execute("INSERT INTO ir_model_data (name,date_init,date_update,module,model,res_id) VALUES (%s, (now() at time zone 'UTC'), (now() at time zone 'UTC'), %s, %s, %s)", \
                    (name_id, module, model, aid)
                )
        cr.commit()

    def deleteDefaultAccountingTemplates(self, cr):
        res = cr.execute("UPDATE account_chart_template SET property_account_receivable = NULL, property_account_payable = NULL, property_account_expense_categ = NULL, property_account_income_categ = NULL WHERE id = 1")
        res = cr.execute("DELETE FROM account_fiscal_position_template")
        res = cr.execute("DELETE FROM account_fiscal_position_tax_template")
        res = cr.execute("DELETE FROM account_tax_code_template")
        res = cr.execute("DELETE FROM account_tax_template")
        res = cr.execute("DELETE FROM account_account_template")
        #res = cr.execute("DELETE FROM account_account")
        res = cr.execute("DELETE FROM account_chart_template")
        cr.commit()
        return True
    
    def _init_currency(self, cr, uid, ids=None, context=None):
        logger.debug('INIT : DISABLE CURRENCIES')    
        cr.execute(""" UPDATE res_currency set active=False where name not in ('USD','TZS','EUR')""")
        return True

isf_tosa_coa()
